import {FETCH_WEATHER, FETCH_ERROR} from '../actions/index';

export default function(state = [], action) {
  switch (action.type) {
    case FETCH_WEATHER:
      if (!state.find((o) => o.city.id === action.payload.data.city.id)) {
        return [action.payload.data, ...state];
      } else {
        return state;
      }
    case FETCH_ERROR:
      alert(`${action.payload}\n
OpenWeatherMap was unable to find your city
Requests should fit one of the following forms
'City'
'City,Country'
'ZIP Code'`);
      return state;
    default:
      return state;
  }
}