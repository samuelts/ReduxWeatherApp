//actions index.js
import axios from 'axios';

const API_KEY = '406100715d7f0e18ed1a6c693ca6bda7';
const API_URL = `https://api.openweathermap.org/data/2.5/forecast?appid=${API_KEY}`
export const FETCH_WEATHER = 'FETCH_WEATHER';
export const FETCH_ERROR = 'FETCH_ERROR';

function fetchWeatherSuccess(response) {
  return {
    type: FETCH_WEATHER,
    payload: response
  };
}

function fetchWeatherError(err) {
  return {
    type: FETCH_ERROR,
    payload: err
  };
}

export function fetchWeather(city) {

  const url = `${API_URL}&q=${city}`

  return function(dispatch) {
    axios.get(url)
    .then((response) => {
      dispatch(fetchWeatherSuccess(response));
    })
    .catch((error) => {
      dispatch(fetchWeatherError(error));
    });
  }
}