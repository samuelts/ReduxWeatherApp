import React, { Component } from 'react';
import SearchBar from '../containers/search-bar';
import WeatherList from '../containers/weather_list';

export default class App extends Component {
  render() {
    return (
      <div>
        <SearchBar />
        <div id='disclaimer'>*note: This is primarily a demonstration of front-end technologies and the accuracy of data returned has not been validated.</div>
        <WeatherList />
      </div>
    );
  }
}
