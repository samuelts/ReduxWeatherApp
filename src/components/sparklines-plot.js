import React from 'react';
import {Sparklines, SparklinesLine, SparklinesReferenceLine} from 'react-sparklines';

function average(data) {
  return data.reduce((t,x) => t+x) / data.length;
}

export default (props) => {
  const color = props.color;
  const data = props.data;
  const units = props.units;
  const avgVal = Math.floor(average(data) * 100) / 100;
  return (
    <div>
      <Sparklines height={120} width={180} data={data}>
        <SparklinesLine color={color} />
        <SparklinesReferenceLine type='avg' />
      </Sparklines>
      <div>{avgVal} {units}</div>
    </div>
  );
}