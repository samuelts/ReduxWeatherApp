import React, {Component} from 'react';
import {connect} from 'react-redux';
import SparklinesPlot from '../components/sparklines-plot';
import GoogleMap from '../components/google-maps';

class WeatherList extends Component {
  renderWeather(cityData) {
    const {name, country} = cityData.city;
    const fullName = `${name}, ${country}`;
    const tempData = cityData.list.map(city => city.main.temp);
    const cTempData = tempData.map(t => t-273.15);
    const fTempData = cTempData.map(c => c * 9/5 + 32);
    const celsius = false;
    const pressureData = cityData.list.map(city => city.main.pressure);
    const humidityData = cityData.list.map(city => city.main.humidity);
    const {lat, lon} = cityData.city.coord;

    return (
      <tr key={cityData.city.id}>
        <td><GoogleMap lat={lat} lon={lon}/></td>
        <td>
          <SparklinesPlot data={fTempData} color={'red'} units='&deg;F'/>
        </td>
        <td>
          <SparklinesPlot data={pressureData} color={'blue'} units='hPa'/>
        </td>
        <td>
        <SparklinesPlot data={humidityData} color={'green'} units='%'/>
        </td>
      </tr>
    );
  }

  render() {
    return (
      <table className='table table-hover'>
        <thead>
          <tr>
            <th>City</th>
            <th>Temperature (&deg;F)</th>
            <th>Pressure (hPa)</th>
            <th>Humidity (%)</th>
          </tr>
        </thead>
        <tbody>
          {this.props.weather.map(this.renderWeather)}
        </tbody>
      </table>
    );
  }
}

function mapStateToProps({weather}) {
  return {weather};
}

export default connect(mapStateToProps)(WeatherList);