# ReduxWeatherApp

A weather web application using Redux/React.

Demo here: https://samuelts.com/ReduxWeatherApp

## Methods

* React/Redux for app framework
* Bootstrap/CSS for app styling
* Axios library for ajax requests
* OpenWeatherMap API for weather data
* Google Maps Embedded API for map display (not React friendly)
* Webpack/Babel for bundling and transpiling

## Images
![main_view](https://raw.githubusercontent.com/safwyls/logos/master/redux_weather_app.png)

## Post Mortem

### Status
Currently this application is capable of searching for a city's weather data with the following constraints:
  * If the same result is requested multiple times it will refrain from producing duplicate data.
  * Sometimes if a city does not have it's own weather data the data returned may be the nearest major city.
  * ZIP Code search will always return the most accurate location.
  * Temperature is currently presented to the user in Fahrenheit but can easily be changed (in code) to provide Celsius, or even Kelvin (native data from OpenWeatherMap)

### Potential Future Improvements
  * Allow user to select units for each data set (Celsius/Fahrenheit, hPa/millibar, etc.)
  * Allow better user interaction with data

### Known Bugs
  * ~~Requesting the same city's data multiple times has unpredictable behavior~~
    - ~~e.g. Returns the previous city's map for a new request, returns the wrong data.~~
    - ~~This may be due to collisions in app state, needs investigation.~~
  * ~~Requesting a city that does not exist in OpenWeatherMap's API will break the app and require a refresh.~~
  * OpenWeatherMap's API is much more successful at returning data for larger cities but begins to struggle with smaller cities that may share a name with another city. For best results search for the nearest major city.
